# contact-list-snapppay

The project is a simple contacts list of passengers.
It is a test application for Snapp-pay company.

## Getting started

After cloning the application you need to run the following commands to start the project:

```
npm install
npm start
```

## Build

In order to build the application you need to run the following commands:

```
npm run build
```

Thank you
