/** @type {import('tailwindcss').Config} */
// eslint-disable-next-line no-undef
module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class',
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    // 008ff8
    colors: {
      white: '#ffffff',
      black: '#000000',
      blue: {
        400: '#60a5fa',
        500: '#3b82f6',
        600: '#008ff8',
        700: '#1d4ed8',
        800: '#1e40af',
        900: '#1e3a8a',
        1000: '#01081E',
      },
      grey: {
        50: '#f9fafb',
        100: '#f3f4f6',
        200: '#e5e7eb',
        300: '#d1d5db',
        400: '#9ca3af',
        500: '#6b7280',
        600: '#4b5563',
        700: '#374151',
        800: '#1f2937',
        900: '#111827',
      },
    },
    fontFamily: {
      sans: ['Graphik', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
    maxWidth: {
      '6/10': '60%',
      '7/10': '70%',
      '8/10': '80%',
      '9/10': '90%',
    },
    extend: {
      spacing: {
        128: '32rem',
        144: '36rem',
        xs: '20rem',
        sm: '24rem',
        md: '28rem',
        lg: '32rem',
      },
    },
  },
  plugins: [],
}
