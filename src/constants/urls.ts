import { AnyObjectType } from '../types/commonTypes'
import { object2queryParams } from '../utils/objects'

export type PassengersParamsType = {
  limit?: number
  skip?: number
} & AnyObjectType

export const getPassengersUrl = (params: PassengersParamsType): string => {
  const { limit = 30, skip = 0, ...otherParams } = params
  const queryParams: string = object2queryParams({
    limit,
    skip,
    ...otherParams,
  })
  return `/passenger${queryParams}`
}

export const getPassengerByIdUrl = (id: string) => {
  return `/passenger/${id}`
}
