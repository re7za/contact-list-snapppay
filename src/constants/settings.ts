type settingType = {
  baseUrl: string
  isDevelopment: boolean
  staticDir: string
}

/**
 * default values
 */

export const setting: settingType = {
  baseUrl: 'http://localhost:1337',
  isDevelopment: process.env.NODE_ENV === 'development',
  staticDir: '/resources/',
}
