import { AnyObjectType } from '../types/commonTypes'

export enum pageNamesEnum {
  HOME = 'HOME',
  CONTACT = 'CONTACT',
}

type MetaDataType = {
  meta_title: string
  meta_description?: string
  meta_url?: string
} & AnyObjectType

type PageMetaDataType = {
  [key in pageNamesEnum]: MetaDataType
}

export const pagesMetaData: PageMetaDataType = {
  HOME: {
    meta_title: 'contacts-list',
    meta_description: 'This page describes a list of Passengers contacts',
    meta_url: 'https://snapppay.ir',
  },
  CONTACT: {
    meta_title: 'contact-page',
    meta_description: 'This page describes a contact in details',
    meta_url: 'https://snapppay.ir',
  },
}
