import { FunctionComponent } from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import HomePage from './views/pages/Home.page'
import ContactPage from './views/pages/Contact.page'
import ReactQueryWrapper from './wrappers/ReactQueryWrapper'
import ThemeWrapper from './wrappers/ThemeWrapper'

const App: FunctionComponent = () => {
  return (
    <ThemeWrapper>
      <ReactQueryWrapper>
        <Router>
          <Routes>
            <Route path='/' element={<HomePage />} />
            <Route path='/contact/:contactId' element={<ContactPage />} />
          </Routes>
        </Router>
      </ReactQueryWrapper>
    </ThemeWrapper>
  )
}

export default App
