export const getQueryParam = (key = '') => {
  if (typeof window === 'undefined' || key === '') return ''
  return new URLSearchParams(window.location.search).get(key) || ''
}
