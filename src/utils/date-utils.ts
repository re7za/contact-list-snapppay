import { monthNames } from '../constants/date'

export const normalizedDate = (rawDate: number): string => {
  if (typeof rawDate !== 'number') return ''
  const date = new Date(rawDate)
  return `${monthNames[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`
}
