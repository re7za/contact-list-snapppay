import { ContactType } from '../types/contact'

const LAST_VIEWED_LIST_LENGTH = 4
const LAST_VIEWED_LS_KEY = 'last_viewed_contacts'

export const addNewContactToLastViewedContactsListInLS = (contact: ContactType) => {
  if (typeof localStorage === 'undefined') return
  const lastViewedContactsFromLS = localStorage.getItem(LAST_VIEWED_LS_KEY) || '[]'
  let lastViewedContacts: ContactType[] = JSON.parse(lastViewedContactsFromLS)

  // Remove if already exists
  let isContactExist = false
  lastViewedContacts = lastViewedContacts.filter((_contact) => {
    if (_contact.id !== contact.id) return true
    isContactExist = true
    return false
  })

  // Remove last contact
  if (!isContactExist && lastViewedContacts.length >= LAST_VIEWED_LIST_LENGTH)
    lastViewedContacts.splice(-1)

  lastViewedContacts.unshift(contact)
  localStorage.setItem(LAST_VIEWED_LS_KEY, JSON.stringify(lastViewedContacts))
}

export const getLastViewedContactsListFromLS = (): ContactType[] => {
  if (typeof localStorage === 'undefined') return []
  const lastViewedContactsFromLS = localStorage.getItem(LAST_VIEWED_LS_KEY) || '[]'
  return JSON.parse(lastViewedContactsFromLS)
}
