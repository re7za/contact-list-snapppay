import { FunctionComponent } from 'react'
import { twMerge } from 'tailwind-merge'
import { ContactType } from '../../../types/contact'
import Text from '../Text'

interface IContactCardProps extends ContactType {
  className?: string
}

const ContactCard: FunctionComponent<IContactCardProps> = (props) => {
  const { className, first_name, last_name, phone, avatar, email, telegram } = props
  const hasExtraInfo = !!(email || telegram)

  return (
    <div
      className={twMerge(
        `border rounded-md border-grey-200 dark:border-grey-800 shadow-lg
        hover:bg-blue-600 hover:bg-opacity-20 transition overflow-hidden
        flex flex-col lg:flex-row`,
        className,
      )}
    >
      <div className='flex gap-x-6 p-4 lg:w-1/2 lg:ml-5'>
        <div
          className={`w-16 h-16 ml-2 rounded-full overflow-hidden 
            border border-grey-300 dark:border-grey-800 shadow-md`}
        >
          <img alt={`${first_name} ${last_name}`} src={avatar} width='100%' height='100%' />
        </div>
        <div className='pt-2 grow'>
          <Text className='text-grey-800 text-md font-medium'>
            {first_name} {last_name}
          </Text>
          <Text className='text-grey-700 text-sm'>{phone}</Text>
        </div>
      </div>
      {hasExtraInfo ? (
        <div className='pt-2 pb-3 px-8 lg:p-4 lg:pt-6'>
          {email ? (
            <div className='flex'>
              <Text className='text-grey-700 text-sm w-20'>email:</Text>
              <Text className='text-blue-600 text-sm'>{email}</Text>
            </div>
          ) : (
            <></>
          )}
          {telegram ? (
            <div className='flex'>
              <Text className='text-grey-700 text-sm w-20'>telegram:</Text>
              <Text className='text-blue-600 text-sm'>{telegram}</Text>
            </div>
          ) : (
            <></>
          )}
        </div>
      ) : (
        <></>
      )}
    </div>
  )
}

export default ContactCard
