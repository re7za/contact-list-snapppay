import { FunctionComponent } from 'react'
import { twMerge } from 'tailwind-merge'
import Text from '../Text'

interface ILabelValueProps {
  className?: string
  label: string
  value: string
}

const LabelValue: FunctionComponent<ILabelValueProps> = (props) => {
  const { label, value, className } = props
  return value ? (
    <div className={twMerge('flex py-0.5', className)}>
      <Text className='text-grey-700 text-sm w-20'>{label}:</Text>
      <Text className='text-blue-600 dark:text-blue-600 text-sm'>{value}</Text>
    </div>
  ) : (
    <></>
  )
}

export default LabelValue
