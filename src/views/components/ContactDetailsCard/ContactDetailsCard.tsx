import { FunctionComponent } from 'react'
import { twMerge } from 'tailwind-merge'
import { ContactType } from '../../../types/contact'
import { normalizedDate } from '../../../utils/date-utils'
import Text from '../Text'
import LabelValue from './LabelValue'

interface IContactDetailsCardProps extends ContactType {
  className?: string
}

const ContactDetailsCard: FunctionComponent<IContactDetailsCardProps> = (props) => {
  const {
    className,
    first_name,
    last_name,
    phone,
    avatar,
    email,
    telegram,
    gender,
    createdAt,
    updatedAt,
    company,
    address,
    note,
  } = props

  const hasUnderNameSection = !!(email || telegram)

  return (
    <div
      className={twMerge(
        `border rounded-md border-grey-200 dark:border-grey-800
        shadow-lg overflow-hidden p-4 md:p-8 
        mx-auto md:w-3/4`,
        className,
      )}
    >
      <div className='mx-auto w-full lg:w-4/5'>
        <div className={'flex flex-col items-center md:flex-row gap-x-6 md:gap-x-20'}>
          <div
            className={`w-40 h-40 ml-2 rounded-full overflow-hidden 
            border border-grey-300 dark:border-grey-800 shadow-md`}
          >
            <img alt={`${first_name} ${last_name}`} src={avatar} width='100%' height='100%' />
          </div>
          <div className='pt-8 md:pt-2 grow w-full md:w-max'>
            <Text className='text-grey-800 text-md font-medium'>
              {first_name} {last_name}
            </Text>
            <Text className='text-grey-700 text-sm'>{phone}</Text>
            <div className='mt-5'>
              <LabelValue label='gender' value={gender} />
            </div>
            {hasUnderNameSection ? (
              <div className='mt-5'>
                <LabelValue label='email' value={email} />
                <LabelValue label='telegram' value={telegram} />
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>
        <div className='mt-5 md:mt-10 flex flex-col md:flex-row'>
          <LabelValue className='mr-11' label='created at' value={normalizedDate(createdAt)} />
          <LabelValue label='updated at' value={normalizedDate(updatedAt)} />
        </div>
        <div className='mt-5'>
          <LabelValue label='company' value={company} />
          <LabelValue label='address' value={address} />
        </div>
        <div className='mt-5 md:mt-10'>
          <Text className='text-grey-700 text-sm w-20'>note:</Text>
          <Text className='text-blue-600 dark:text-blue-600 text-sm whitespace-pre-wrap mt-2'>
            {note}
          </Text>
        </div>
      </div>
    </div>
  )
}

export default ContactDetailsCard
