import { FunctionComponent, InputHTMLAttributes } from 'react'
import { twMerge } from 'tailwind-merge'

interface ITextFieldProps extends InputHTMLAttributes<Element> {
  className?: string
  label?: string
}

const TextField: FunctionComponent<ITextFieldProps> = (props) => {
  const { type = 'text', name, label, value = '', ...otherProps } = props
  const empty = value === ''

  return (
    <div className='h-14 flex flex-col justify-end'>
      <div className='relative border border-grey-500 dark:border-grey-700 rounded mb-2 w-xs md:w-md m-auto'>
        <input
          className={twMerge(
            'outline-none w-full rounded bg-white dark:bg-blue-1000 dark:text-white text-sm transition-all duration-200 ease-in-out p-2',
            empty ? '' : 'pt-6',
          )}
          type={type}
          id={name}
          name={name}
          value={value}
          {...otherProps}
        />
        <label
          className={[
            'absolute top-0 left-0 flex items-center opacity-50 p-2 transition-all duration-1 ease-in-out dark:text-grey-100',
            empty ? 'text-sm' : 'text-xs',
          ].join(' ')}
          htmlFor={name}
        >
          {label}
        </label>
      </div>
    </div>
  )
}

export default TextField
