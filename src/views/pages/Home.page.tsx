import { FunctionComponent } from 'react'
import { pageNamesEnum } from '../../constants/pagesMetaData'
import GeneralPageWrapper from '../../wrappers/GeneralPageWrapper'
import HomePageTemplate from '../templates/HomePageTemplate'

interface IHomePageProps {}

const HomePage: FunctionComponent<IHomePageProps> = () => {
  return (
    <GeneralPageWrapper pageName={pageNamesEnum.HOME}>
      <HomePageTemplate />
    </GeneralPageWrapper>
  )
}

export default HomePage
