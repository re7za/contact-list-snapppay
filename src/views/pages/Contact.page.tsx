import { FunctionComponent } from 'react'
import { pageNamesEnum } from '../../constants/pagesMetaData'
import GeneralPageWrapper from '../../wrappers/GeneralPageWrapper'
import ContactPageTemplate from '../templates/ContactPageTemplate'

interface IContactPageProps {}

const ContactPage: FunctionComponent<IContactPageProps> = () => {
  return (
    <GeneralPageWrapper pageName={pageNamesEnum.CONTACT}>
      <ContactPageTemplate />
    </GeneralPageWrapper>
  )
}

export default ContactPage
