import { FunctionComponent, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { ContactType } from '../../../types/contact'
import {
  addNewContactToLastViewedContactsListInLS,
  getLastViewedContactsListFromLS,
} from '../../../utils/contacts-utils'
import ContactCard from '../../components/ContactCard'
import Text from '../../components/Text'

interface ILastViewedContactsListProps {}

const LastViewedContactsList: FunctionComponent<ILastViewedContactsListProps> = () => {
  const [lastViewedList, setLastViewedList] = useState<ContactType[]>([])

  const handleContactClick = (contact: ContactType) => {
    addNewContactToLastViewedContactsListInLS(contact)
  }

  useEffect(() => {
    const cachedContactsList = getLastViewedContactsListFromLS()
    setLastViewedList(cachedContactsList)
  }, [])

  if (lastViewedList.length === 0) return <></>
  return (
    <div className='m-auto sm:w-sm md:w-md lg:w-3/5'>
      <Text className='mb-2'>Last viewed contacts</Text>
      {lastViewedList.map((contact: ContactType) => (
        <Link
          key={contact.id}
          to={`/contact/${contact.id}/`}
          onClick={() => handleContactClick(contact)}
          className='w-full'
        >
          <ContactCard {...contact} className='my-4' />
        </Link>
      ))}
    </div>
  )
}

export default LastViewedContactsList
