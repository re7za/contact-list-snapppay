import { FunctionComponent, useContext, useEffect, useState } from 'react'
import { ContactsContext } from '../../../contexts/ContactsContext'
import useDebounce from '../../../hooks/useDebounce'
import TextField from '../../components/TextField'

interface IContactsSearchFieldProps {}

const ContactsSearchField: FunctionComponent<IContactsSearchFieldProps> = () => {
  const { state, actions, dispatch } = useContext(ContactsContext)
  const { searchString } = state

  const [searchText, setSearchText] = useState<string>(searchString)
  const debouncedSearchText = useDebounce(searchText)

  const handleTextFieldChange = (e: any) => {
    const value = e.target.value
    setSearchText(value)
  }

  useEffect(() => {
    actions.updateSearchString(debouncedSearchText)(dispatch)
  }, [debouncedSearchText])

  return (
    <div className='py-8'>
      <TextField
        name='search-field'
        label='Search first name or phone number..'
        value={searchText}
        onChange={handleTextFieldChange}
      />
    </div>
  )
}

export default ContactsSearchField
