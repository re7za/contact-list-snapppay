import { FunctionComponent, useContext, useEffect, useMemo } from 'react'
import { Link } from 'react-router-dom'
import { ContactsContext } from '../../../contexts/ContactsContext'
import { useGetPassengersList } from '../../../hooks/services/useGetPassengersList'
import { ContactType } from '../../../types/contact'
import { addNewContactToLastViewedContactsListInLS } from '../../../utils/contacts-utils'
import Pagination from '../../components/Pagination'
import ContactCard from '../../components/ContactCard'
import { ITEMS_PER_PAGE } from './constants'
import { getWhereQueryParam } from './utils'
import Text from '../../components/Text'
import usePage from '../../../hooks/usePage'

interface IContactsListProps {}

const ContactsList: FunctionComponent<IContactsListProps> = () => {
  const { state } = useContext(ContactsContext)
  const { searchString } = state

  const [page, setPage] = usePage()

  const memoizedWhereParam = useMemo(() => getWhereQueryParam(searchString), [searchString])

  const { data, isLoading, isError } = useGetPassengersList(
    {
      limit: ITEMS_PER_PAGE,
      skip: (page - 1) * ITEMS_PER_PAGE,
      ...(memoizedWhereParam ? { where: memoizedWhereParam } : {}),
    },
    {
      refetchInterval: 10 * 60 * 1000, // 10 minutes
      keepPreviousData: true,
      refetchOnWindowFocus: false,
    },
  )

  const handleContactClick = (contact: ContactType) => {
    addNewContactToLastViewedContactsListInLS(contact)
  }

  const handlePageChange = (page: number) => {
    setPage(page)
  }

  useEffect(() => {
    if (memoizedWhereParam !== '') setPage(1)
  }, [memoizedWhereParam])

  if (isLoading) return <div>fancy loading..</div>
  if (isError) return <div>handle error somehow!</div>

  const { meta, items } = data
  const totalPages: number = Math.ceil(meta.total / meta.limit) || 1

  return (
    <Pagination
      onPageChange={handlePageChange}
      totalPages={totalPages}
      currentPage={page}
      className='py-8'
    >
      <div className='m-auto sm:w-sm md:w-md lg:w-3/5'>
        <Text className='mb-2'>All contacts</Text>
        {items?.map((contact: ContactType) => (
          <Link
            key={contact.id}
            to={`/contact/${contact.id}/`}
            onClick={() => handleContactClick(contact)}
            className='w-full'
          >
            <ContactCard {...contact} className='my-4' />
          </Link>
        ))}
      </div>
    </Pagination>
  )
}

export default ContactsList
