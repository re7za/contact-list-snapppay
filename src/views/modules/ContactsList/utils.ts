export const getStringified = (obj: any) => JSON.stringify(obj)

export const getWhereQueryParam = (str = ''): string => {
  if (str === '') return ''
  if (!isNaN(Number(str))) return getStringified({ phone: { contains: str } })
  return getStringified({ first_name: { contains: str } })
}
