import { FunctionComponent } from 'react'
import { useParams } from 'react-router-dom'
import { useGetPassenger } from '../../../hooks/services/useGetPassenger'
import ContactDetailsCard from '../../components/ContactDetailsCard'
import Container from '../../layouts/Container'

interface IContactPageTemplateProps {}

const ContactPageTemplate: FunctionComponent<IContactPageTemplateProps> = () => {
  const { contactId } = useParams()

  const { data, isLoading, isError } = useGetPassenger(contactId || '', {
    keepPreviousData: true,
    refetchOnWindowFocus: false,
  })

  if (isLoading) return <div>fancy loading..</div>
  if (isError) return <div>handle error somehow!</div>

  return (
    <Container className='pt-10'>
      <ContactDetailsCard {...data} />
    </Container>
  )
}

export default ContactPageTemplate
