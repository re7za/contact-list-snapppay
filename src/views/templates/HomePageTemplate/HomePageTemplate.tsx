import { FunctionComponent, useReducer } from 'react'
import {
  ContactsContext,
  contactsInitialState,
  contactsReducer,
  contactsActions,
} from '../../../contexts/ContactsContext'
import Container from '../../layouts/Container'
import ContactsList from '../../modules/ContactsList'
import ContactsSearchField from '../../modules/ContactsSearchField'
import LastViewedContactsList from '../../modules/LastViewedContactsList'

interface IHomePageTemplateProps {}

const HomePageTemplate: FunctionComponent<IHomePageTemplateProps> = () => {
  const [contactsState, contactsDispatch] = useReducer(contactsReducer, contactsInitialState)
  const { searchString } = contactsState
  return (
    <>
      <ContactsContext.Provider
        value={{
          state: contactsState,
          dispatch: contactsDispatch,
          actions: contactsActions,
        }}
      >
        <Container>
          <ContactsSearchField />
          {searchString === '' ? (
            <div className='mb-20'>
              <LastViewedContactsList />
            </div>
          ) : (
            <></>
          )}
          <ContactsList />
        </Container>
      </ContactsContext.Provider>
    </>
  )
}

export default HomePageTemplate
