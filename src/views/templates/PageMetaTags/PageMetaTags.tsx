import { FunctionComponent } from 'react'
import { pagesMetaData, pageNamesEnum } from '../../../constants/pagesMetaData'
import { setting } from '../../../constants/settings'
import { Helmet, HelmetProvider } from 'react-helmet-async'

export interface IPageMetaTagsProps {
  pageName: pageNamesEnum
}

const PageMetaTags: FunctionComponent<IPageMetaTagsProps> = (props) => {
  const { pageName } = props
  const constantMetaData = pagesMetaData[pageName] || {}

  const pageTitle: string = constantMetaData.meta_title
  const pageDescription: string = constantMetaData.meta_title

  const logoSrc = setting.staticDir + `images/snapppay-logo.jpeg`

  return (
    <HelmetProvider>
      <Helmet>
        <meta charSet='utf-8' />
        <title>{pageTitle}</title>
        <meta name='description' content={pageDescription} />
        <link rel='icon' href={logoSrc} />
        <link rel='apple-touch-icon' href={logoSrc} />
        <meta
          name='viewport'
          content='width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no'
        />
        <meta name='theme-color' content='#008ff8' />
      </Helmet>
    </HelmetProvider>
  )
}

export default PageMetaTags
