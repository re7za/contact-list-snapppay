import { createContext } from 'react'
import * as types from './types'
import * as contactsActions from './Action'
// import { getPageFromUrl } from './utils'

export type ContactsStateTypes = {
  searchString: string
  // page: number
}

export const contactsInitialState: ContactsStateTypes = {
  searchString: '',
  // page: getPageFromUrl(),
}

export const contactsReducer = (
  state: ContactsStateTypes = contactsInitialState,
  action: types.contactsActionsTypes,
) => {
  const { type, payload } = action
  switch (type) {
    // case types.UPDATE_PAGE: {
    //   const { page } = payload
    //   return {
    //     ...state,
    //     page,
    //   }
    // }
    case types.UPDATE_SEARCH_QUERY: {
      const { searchString } = payload
      return {
        ...state,
        // ...(searchString === '' ? {} : { page: 1 }), // ignore if empty
        searchString,
      }
    }
    default:
      throw new Error()
  }
}

const ContactsContext = createContext({
  state: contactsInitialState,
  dispatch: {} as types.ContactsDispatchType,
  actions: contactsActions,
})
export default ContactsContext
