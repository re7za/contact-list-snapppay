import { getQueryParam } from '../../utils/location'

export const getPageFromUrl = (): number => {
  const value = getQueryParam('page')
  const page = Number(value)
  if (value === '' || isNaN(page)) return 1
  return page
}
