import * as types from './types'

// export const updatePage = (page: number) => (dispatch: types.ContactsDispatchType) => {
//   return dispatch({ type: types.UPDATE_PAGE, payload: { page } })
// }

export const updateSearchString =
  (searchString: string) => (dispatch: types.ContactsDispatchType) => {
    return dispatch({ type: types.UPDATE_SEARCH_QUERY, payload: { searchString } })
  }
