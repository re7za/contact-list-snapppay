import ContactsContext, { contactsInitialState, contactsReducer } from './Reducer'
import * as contactsActions from './Action'

export { ContactsContext, contactsInitialState, contactsReducer, contactsActions }
