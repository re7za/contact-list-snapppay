import { Dispatch } from 'react'

export type contactsActionsTypes = {
  type: 'UPDATE_SEARCH_QUERY'
  payload: { searchString: string }
}
// | {
//     type: 'UPDATE_PAGE'
//     payload: { page: number }
//   }

export type ContactsDispatchType = Dispatch<contactsActionsTypes>

export const UPDATE_PAGE = 'UPDATE_PAGE'
export const UPDATE_SEARCH_QUERY = 'UPDATE_SEARCH_QUERY'
