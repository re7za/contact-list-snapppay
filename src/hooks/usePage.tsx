import { useState } from 'react'
import { useSearchParams } from 'react-router-dom'

type UsePageReturnType = [number, (newPage: number) => void]

const usePage = (defaultPage = 1): UsePageReturnType => {
  const [searchParams, setSearchParams] = useSearchParams()
  const urlPage = searchParams.get('page')

  const [page, setPage] = useState(urlPage || defaultPage)

  const handleNewPage = (newPage: number) => {
    setPage(newPage)
    setSearchParams(`page=${newPage}`)
  }

  return [Number(page), handleNewPage]
}

export default usePage
