import { AxiosResponse } from 'axios'
import { useQuery } from 'react-query'
import { PassengersParamsType, getPassengersUrl } from '../../constants/urls'
import { AnyObjectType } from '../../types/commonTypes'
import { request } from '../../utils/axios-utils'
import { UseQueryOptionsType } from './types'

export const PASSENGERS_LIST_KEY = 'passengers-list'

export const fetchPassengersList = (queryParams: PassengersParamsType): Promise<AxiosResponse> => {
  const url: string = getPassengersUrl(queryParams)
  return request(url)
}

const getPassengersCacheKey = (params: AnyObjectType) => {
  const paramsKeys = Object.keys(params)
  const paramsCacheKeys = paramsKeys.map((key) => `${key}-${params?.[key]}`)
  return [PASSENGERS_LIST_KEY, ...paramsCacheKeys].join(',')
}

export const useGetPassengersList = (
  params: PassengersParamsType,
  options?: UseQueryOptionsType,
) => {
  const cacheKey = getPassengersCacheKey(params)
  return useQuery(cacheKey, () => fetchPassengersList(params), options)
}
