import { AxiosResponse } from 'axios'
import { useQuery } from 'react-query'
import { getPassengerByIdUrl } from '../../constants/urls'
import { request } from '../../utils/axios-utils'
import { UseQueryOptionsType } from './types'

export const contactKey = 'contact'

export const fetchPassenger = (id: string): Promise<AxiosResponse> => {
  const url: string = getPassengerByIdUrl(id)
  return request(url)
}

export const useGetPassenger = (contactId: string | number, options?: UseQueryOptionsType) => {
  const cacheKey = [contactKey, contactId].join(',')
  return useQuery(cacheKey, () => fetchPassenger(contactId as string), options)
}
