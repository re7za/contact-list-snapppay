import { UseQueryOptions } from 'react-query'
import { AnyObjectType } from '../../types/commonTypes'
import { ContactType } from '../../types/contact'

export type UseQueryOptionsType = Omit<
  UseQueryOptions<unknown, unknown, any, string>,
  'queryKey' | 'queryFn'
>

type PassengersMetaType = {
  skipped: number
  limit: number
  total: number
  criteria?: AnyObjectType[]
}

export type PassengersDataType = {
  meta: PassengersMetaType
  items?: ContactType[]
}
