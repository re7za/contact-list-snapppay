import { FunctionComponent, ReactNode, useRef } from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import { setting } from '../../constants/settings'

interface IReactQueryWrapperProps {
  children: ReactNode
}

const ReactQueryWrapper: FunctionComponent<IReactQueryWrapperProps> = (props) => {
  const { children } = props
  const queryClient = useRef(new QueryClient())

  return (
    <>
      <QueryClientProvider client={queryClient.current}>
        {children}
        {setting.isDevelopment ? <ReactQueryDevtools initialIsOpen={false} /> : <></>}{' '}
      </QueryClientProvider>
    </>
  )
}

export default ReactQueryWrapper
