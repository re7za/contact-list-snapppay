import { FunctionComponent, ReactNode } from 'react'
import PageMetaTags, { IPageMetaTagsProps } from '../../views/templates/PageMetaTags'

interface IGeneralPageWrapperProps extends IPageMetaTagsProps {
  children?: ReactNode
}

const GeneralPageWrapper: FunctionComponent<IGeneralPageWrapperProps> = (props) => {
  const { pageName, children } = props
  return (
    <>
      <PageMetaTags pageName={pageName} />
      {children}
    </>
  )
}

export default GeneralPageWrapper
